#!/bin/sh
apt update
apt install zip
zip -r sn-monokai-ristretto.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-monokai-ristretto.zip public/
cd public
unzip sn-monokai-ristretto.zip
